// start of function for slidedown in filter-side
$(function() {
    $('.fil-age').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next()
                .stop()
                .slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next()
                .stop()
                .slideDown(300);
        }
    });
});

$(function() {
    $('.fil-county').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next()
                .stop()
                .slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next()
                .stop()
                .slideDown(300);
        }
    });
});

$(function() {
    $('.fil-marital').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next()
                .stop()
                .slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next()
                .stop()
                .slideDown(300);
        }
    });
});
// end of function for slidedown in filter-side

//when window refresh
$(document).ready(function() {

    emptylocalstorage();

    var x = JSON.parse(localStorage.getItem('users'));

    createTable(x.splice(0, Number($("#perpage-number").val())));

    for (var i = 0; i < Number($("#perpage-number").val()); i++) {
        $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(i + 1);
    };


    var m = 1;
    addpagebutton(m, x);

    hidebuttons();



    // $('.chips').material_chip();
});

function hidebuttons() {
    $("#tbody tr").on("mouseenter", function() {
        $(this).find("td:eq(7)").css("display", "inline-block");

    });
    $("#tbody tr").on("mouseleave", function() {
        $(this).find("td:eq(7)").css("display", "none")
    });
}







function addpagebutton(m, x) {

    var x = JSON.parse(localStorage.getItem('users'));

    var numItems = x.length;
    var perPage = Number($("#perpage-number").val());

    $("#pagebutton").pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        firstText: "first",
        lastText: "last",
        currentPage: m,
        onPageClick: function(pageNumber) {

            var pageNumber = $(".current").not(".prev").not(".first").not(".next").not(".last").text();
            console.log(pageNumber)
            var showFrom = perPage * (pageNumber - 1);

            var showTo = showFrom + perPage;

            var filterpagination = x.slice(showFrom, showTo);

            createTable(filterpagination);

            hidebuttons();

            for (var i = 0; i < x.length; i++) {
                $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(perPage * (pageNumber - 1) + i + 1);
            }

        }

    });

}






//end of function for pagination

$(document).on('change', '#perpage-number', function editbtn() {

    var x = JSON.parse(localStorage.getItem('users'));

    createTable(x.splice(0, Number($("#perpage-number").val())));

    for (var i = 0; i < Number($("#perpage-number").val()); i++) {
        $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(i + 1);
    };


    var m = 1;
    addpagebutton(m, x);

    hidebuttons();

});


//for if window refresh and local is empty this function apply
function emptylocalstorage() {
    var x = JSON.parse(localStorage.getItem('users'));
    if (x == null) {
        localStorage.setItem('users', "[]");;
    }
}




$("#mymodal").on("click", function() {
    $("#myModalHorizontal").modal('show');
});




//start of submit function
$("#submit").on("click", function() {

    var name = $("#inputname").val();
    var familyname = $("#inputfamily").val();
    var age = $("#inputage").val();
    var job = $("#inputjob").val();
    var sport = $("#inputsport").val();
    var number = $("#inputnumber").val();
    var birthsatate = $("#inputstate").val();
    var birthcounty = $("#inputcounty").val();
    var des = $("#inputdescription").val();

    var user = {

        "nameuser": name,
        "familyuser": familyname,
        "ageuser": age,
        "maritaluser": $('input[name=maritalRadios]:checked').val(),
        "jobuser": job,
        "sportuser": sport,
        "numberuser": number,
        "birthstateuser": birthsatate,
        "birthcountyuser": birthcounty,
        "descriptionuser": des,
    };

    if (totalvalidation(user)) {

        var userarray = JSON.parse(localStorage.getItem('users'));

        userarray.push(user);

        localStorage.setItem('users', JSON.stringify(userarray));

        var x = JSON.parse(localStorage.getItem('users'));

        var pageNumber = $(".current").not(".prev").not(".first").not(".next").not(".last").text();

        var perPage = Number($("#perpage-number").val());

        var showFrom = perPage * (pageNumber - 1);

        var showTo = showFrom + perPage;

        var filterpagination = x.slice(showFrom, showTo);

        createTable(filterpagination);

        addpagebutton(pageNumber, x);

        hidebuttons();

        for (var i = 0; i < x.length; i++) {
            $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(perPage * (pageNumber - 1) + i + 1);
        }

        emptyinputs();

        $("#myModalHorizontal").modal('hide')
    } else {
        $("#submit").attr("data-dismiss", "");
    }
});
//end of submit function

// start of function for birthstate dropdown
$("#close").on("click", function() {
    emptyinputs();
});

var birthcounty = {};
birthcounty['تهران'] = ['ملارد', 'قدس', 'اسلامشهر'];
birthcounty['آذربایجان شرقی'] = ['تبریز', 'مراغه', 'مرند'];
birthcounty['مازندران'] = ['آمل', 'بابل', 'رامسر'];
birthcounty['یزد'] = ['اردلان', 'میبد', 'انار'];
birthcounty['فارس'] = ['فسا', 'مرودشت', 'جهرم'];

function Changestatelist() {
    var countylist = document.getElementById("inputcounty");
    var countyselect = $("#inputstate").val();
    while (countylist.options.length) {
        countylist.remove(0);
    }

    var birthplace = birthcounty[countyselect];

    if (birthplace) {
        var i;
        for (i = 0; i < birthplace.length; i++) {

            var x = new Option(birthplace[i]);

            countylist.options.add(x);
        }
    }
}
// end of function for birthstate dropdown

//for show close button on modal
$("#mymodal").on("click", function() {
    $("#close").show();
});


//for conver birthday date to age
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);

    var age = today.getFullYear() - birthDate.getFullYear();

    var m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

//for empty input of form
function emptyinputs() {
    $("#inputname").val('');
    $("#inputfamily").val('');
    $("#inputage").val('');
    $("#inputsport").val('');
    $("#inputdescription").val('');
    $("#inputjob").val('');
    $("#inputnumber").val('');
    $("#inputstate").val('');
    $("#inputcounty").val('');
    $("input[name=maritalRadios]").prop("checked", false);
}

//for create table
function createTable(x) {
    $("#table tbody").html("");


    for (var i in x) {
        createRow(x[i]);
    }
}
//for creat row
function createRow(data) {
    $("#table tbody").append(`<tr>
<td style="border: none;padding: 8px 0px 8px 0px;"></td>
<td style="border: none;padding: 8px 0px 8px 0px;">${data.nameuser}</td>
<td style="border: none;padding: 12px 0px 12px 0px;">${data.familyuser}</td>
<td style="border: none;padding: 12px 0px 12px 0px;">${getAge(data.ageuser)}</td>
<td style="border: none;padding: 12px 0px 12px 0px;">${data.jobuser}</td>
<td style="border: none;padding: 12px 0px 12px 0px;">${data.numberuser}</td>
<td style="border: none;padding: 12px 0px 12px 0px;">${data.maritaluser}</td>
<td style="border: none;padding: 12px 0px 12px 0px;display:none;">
<a class="view"><span class="material-icons md-24" style="color:#ceced1;">visibility</span></a>
<a class="delete"><span class="material-icons md-24" style="color:#ceced1;">delete_outline</span></a>
<a class="edit"><span class="material-icons md-24" style="color:#ceced1;">edit</span></a>
</td>
</tr>`);

    $("tr").last().data("name", data.nameuser);
    $("tr").last().data("family", data.familyuser);
    $("tr").last().data("age", data.ageuser);
    $("tr").last().data("marital", data.maritaluser);
    $("tr").last().data("job", data.jobuser);
    $("tr").last().data("sport", data.sportuser);
    $("tr").last().data("number", data.numberuser);
    $("tr").last().data("birthsatate", data.birthstateuser);
    $("tr").last().data("birthcounty", data.birthcountyuser);
    $("tr").last().data("description", data.descriptionuser);
}

//function for validation
function limitage(data) {

    var validage = true;

    if (getAge(data.ageuser) < 18) {

        validage = false;

        Command: toastr["error"]("age is low")

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

    }
    return validage;
}

function equalnumber() {

    var x = JSON.parse(localStorage.getItem('users'));

    var validnumber = true;

    for (var i = 0; i < x.length; i++) {

        if ($("#inputnumber").val() == x[i].numberuser) {

            validnumber = false;

            Command: toastr["error"]("exist two equal number")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    }

    return validnumber;
}

function validation(data) {

    var validempty = true;

    if (data.nameuser == "" || data.familyuser == "" || data.ageuser == "" || data.maritaluser == undefined || data.jobuser == null || data.sportuser == null || data.numberuser == "" || isNaN(data.numberuser) || data.birthstateuser == null || data.birthcountyuser == null) {

        validempty = false;

        Command: toastr["error"]("there is issue on fields")

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }
    return validempty;
}

function totalvalidation(data) {

    var resultage = limitage(data);

    var resultnumber = equalnumber();

    var resultvalidation = validation(data);

    if (resultage == false || resultnumber == false || resultvalidation == false) {

        return false;
    } {
        return true;
    }
}



var selectedclass = "";
var y = "";
$(document).on('click', 'a.edit', function editbtn() {

    y = $(this).closest('tr').data("number");

    var getname = $(this).parents("tr").data("name");
    $("#inputname").val(getname);
    var getfamily = $(this).parents("tr").data("family");
    $("#inputfamily").val(getfamily);
    var getdate = $(this).parents("tr").data("age");
    $("#inputage").val(getdate);
    var getjob = $(this).parents("tr").data("job");
    $("#inputjob").val(getjob);
    var getsport = $(this).parents("tr").data("sport");
    $("#inputsport").val(getsport);
    var getnumber = $(this).parents("tr").data("number");
    $("#inputnumber").val(getnumber);
    var getbirthsatate = $(this).parents("tr").data("birthsatate");
    $("#inputstate").val(getbirthsatate);
    var getbirthcounty = $(this).parents("tr").data("birthcounty");
    $("#inputcounty").val(getbirthcounty);
    var getdes = $(this).parents("tr").data("description");
    $("#inputdescription").val(getdes);
    var getvaluemarital = $(this).parents("tr").data("marital");
    $("input[name=maritalRadios][value=" + getvaluemarital + "]").prop('checked', true);

    selectedclass = $(this).parents("tr");

    $(selectedclass).addClass("selected");

    $("#myModalHorizontal").modal('show');

    $("#submit").parent("div").append(`<button type="submit" class="btn btn-primary save" style="margin-right: 15px; display:inline-block;">ذخیره</button><button type="submit" class="btn btn-default cancel" style="display:inline-block">انصراف</button>`)


    $("#close").hide();
    $("#submit").hide();
    $(".edit").hide();
    $(".view").hide();
    $(".delete").hide();

});


function totalvalidation1(data) {
    var resultage = limitage(data);
    var resultnumber1 = equalnumber1();
    var resultvalidation = validation(data);

    if (resultage == false || resultnumber1 == false || resultvalidation == false) {

        return false;

    } {
        return true;
    }
}

function equalnumber1() {
    var x = JSON.parse(localStorage.getItem('users'));
    var validnumber = true;
    for (var i = 0; i < x.length; i++) {
        if (x[i].numberuser == y) {
            continue;
        }
        if ($("#inputnumber").val() == x[i].numberuser) {

            validnumber = false;
            Command: toastr["error"]("exist two equal number")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,

                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    }
    return validnumber;
}


$(document).on("click", ".save", function() {

    var returnname = $("#inputname").val();
    var returnfamilyname = $("#inputfamily").val();
    var returnage = $("#inputage").val();
    var returnjob = $("#inputjob").val();
    var returnsport = $("#inputsport").val();
    var returnbirthsatate = $("#inputstate").val();
    var returnbirthcounty = $("#inputcounty").val();
    var returnnumber = $("#inputnumber").val();
    var returndes = $("#inputdescription").val();


    var userinfo = {
        "nameuser": returnname,
        "familyuser": returnfamilyname,
        "ageuser": returnage,
        "jobuser": returnjob,
        "maritaluser": $('input[name=maritalRadios]:checked').val(),
        "sportuser": returnsport,
        "birthstateuser": returnbirthsatate,
        "birthcountyuser": returnbirthcounty,
        "numberuser": returnnumber,
        "descriptionuser": returndes,
    };

    if (totalvalidation1(userinfo)) {

        var x = JSON.parse(localStorage.getItem('users'));

        for (var i = 0; i < x.length; i++) {

            if (x[i].numberuser == y) {

                x[i] = userinfo;
            }
        }

        localStorage.setItem('users', JSON.stringify(x));

        x = JSON.parse(localStorage.getItem('users'));

        var pageNumber = $(".current").not(".prev").not(".first").not(".next").not(".last").text();

        var perPage = Number($("#perpage-number").val());

        var showFrom = perPage * (pageNumber - 1);

        var showTo = showFrom + perPage;

        var filterpagination = x.slice(showFrom, showTo);

        createTable(filterpagination);

        addpagebutton(pageNumber, x);

        hidebuttons();

        for (var i = 0; i < x.length; i++) {
            $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(perPage * (pageNumber - 1) + i + 1);
        }

        $("#myModalHorizontal").modal('hide');
        $("#submit").show();

        $(".save").remove();
        $(".cancel").remove();
        $(".edit").show();
        $(".delete").show();
        $(".view").show();

        emptyinputs();

        $(selectedclass).removeClass();

    }

});


$(document).on('click', 'a.delete', function() {

    var y = $(this).closest('tr').data("number");

    Swal.fire({
        title: 'آیا مطمئن هستید؟',
        // text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonText: "خیر",
        cancelButtonColor: '#d33',
        confirmButtonText: 'بله حذف کنید!'
    }).then((result) => {
        if (result.value) {
            var x = JSON.parse(localStorage.getItem('users'));
            for (var i = 0; i < x.length; i++) {
                if (x[i].numberuser == y) {

                    x.splice(i, 1);

                }
            }

            localStorage.setItem('users', JSON.stringify(x));

            var x = JSON.parse(localStorage.getItem('users'));

            var pageNumber = $(".current").not(".prev").not(".first").not(".next").not(".last").text();

            var perPage = Number($("#perpage-number").val());

            var showFrom = perPage * (pageNumber - 1);

            var showTo = showFrom + perPage;

            var filterpagination = x.slice(showFrom, showTo);

            createTable(filterpagination);

            addpagebutton(pageNumber, x);

            hidebuttons();

            for (var i = 0; i < x.length; i++) {
                $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(perPage * (pageNumber - 1) + i + 1);
            }

            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',

            )

        }
    })


});


$(document).on("click", ".cancel", function() {

    $("#submit").show();
    $(".save").remove();
    $(".cancel").remove();
    $(".edit").show();
    $(".delete").show();
    $(".view").show();

    $("#myModalHorizontal").modal("hide");
    emptyinputs();
    $(selectedclass).removeClass("selected");
});





$(document).on("click", "a.view", function() {
    y = $(this).closest('tr').data("number");

    var getname = $(this).parents("tr").data("name");
    $(".name-view").text(getname)
    var getfamily = $(this).parents("tr").data("family");
    $(".familyname-view").text(getfamily);
    var getdate = $(this).parents("tr").data("age");
    $(".age-view").text(getdate);
    var getjob = $(this).parents("tr").data("job");
    $(".job-view").text(getjob);
    var getsport = $(this).parents("tr").data("sport");
    $(".sport-view").text(getsport);
    var getnumber = $(this).parents("tr").data("number");
    $(".number-view").text(getnumber);
    var getbirthsatate = $(this).parents("tr").data("birthsatate");
    $(".state-view").text(getbirthsatate);
    var getbirthcounty = $(this).parents("tr").data("birthcounty");
    $(".county-view").text(getbirthcounty);
    var getdes = $(this).parents("tr").data("description");
    $(".des-view").text(getdes);
    var getvaluemarital = $(this).parents("tr").data("marital");
    $(".marital-view").text(getvaluemarital);


    $("#modal-view").modal('show');

    $("#submit").hide();


});



//start for age-filter rangeslider
function fixPosition(slider, fromPercent) {
    let w;

    if (fromPercent === 100) {
        w = 'calc(100%)';
    } else {
        w = 'calc(' + fromPercent + '% - 12px)';
    }

    slider.find('.irs-bar').css('left', 'auto').css('width', w);
    slider.find('.irs-handle').css('left', 'auto').css('right', w);
}

$(document).ready(function() {
    let min = 14;
    let max = 45;

    $(".js-range-slider").ionRangeSlider({
        type: 'single',
        skin: 'round',
        from_fixed: true,
        min: min,
        max: max,
        grid: true,
        grid_num: 3,
        grid_snap: false,
        grid_snap: true,
        grid_margin: false,
        hide_min_max: true,
        hide_from_to: true,


        onStart: function(data) {
            fixPosition(data.slider, 0);
        },

        onChange: function(data) {
            let value = $('#range-slider').val();

            $('#range-slide-value').text(max + min - value);
            data.from = min + (max - data.from);
            data.from_percent = 100 - data.from_percent;

            fixPosition(data.slider, data.from_percent);

        },

        prettify: function(num) {
            num = min + (max - num);
            return num;
        }
    });

    $('#range-slide-get-value').click(function() {
        let value = $('#range-slider').val();

        $('#range-slide-value').text(value);
    });
});
//end for age-filter rangeslider


var typingTimer;
var doneTypingInterval = 1000;

$('.text-search').keyup(function() {
    clearTimeout(typingTimer);
    if ($('.text-search').val()) {
        typingTimer = setTimeout(search, doneTypingInterval);
    }
});

//user is "finished typing," do something


//start function search
function search() {

    var x = JSON.parse(localStorage.getItem('users'));
    var inputsearch = $(".text-search").val().toLowerCase();

    var searchnumberarray = [];
    if (inputsearch == "") {
        $("#table tbody").html("");
        $("#pagebutton").html("");
        return false;
    }

    for (var i = 0; i < x.length; i++) {

        if (x[i].nameuser.toLowerCase().indexOf(inputsearch) > -1 ||
            x[i].familyuser.toLowerCase().indexOf(inputsearch) > -1 ||
            getAge(x[i].ageuser).toString().indexOf(inputsearch) > -1 ||
            x[i].maritaluser.toLowerCase().indexOf(inputsearch) > -1 ||
            x[i].numberuser.indexOf(inputsearch) > -1 ||
            x[i].jobuser.indexOf(inputsearch) > -1) {

            searchnumberarray.push(x[i]);

        }
    }
    $("#table tbody").html("");
    $("#pagebutton").html("");

    createTable(searchnumberarray);

    hidebuttons();

    for (var i = 0; i < searchnumberarray.length; i++) {
        $("#tbody").find("tr:eq(" + i + ")").find("td:eq(0)").text(i + 1);
    }
}


//end function search